// import the fetch
const fetch = require('node-fetch');

const userURL = 'https://jsonplaceholder.typicode.com/users';
const todosURL = 'https://jsonplaceholder.typicode.com/todos';

// fetch the data from userURL
fetch(userURL)
.then( (response) => {
    // if the response is not ok throw the error
    if(!response.ok){
        throw new Error(`Failed to fetch users: ${response.statusText}`);
    }
    // if response was ok then convert data in json and return
    return response.json();
})
.then((data) => {
    console.log("Users Data :", data);

    // fetch the data from todosURL
    return fetch(todosURL)
})
.then((response) => {
    // if the response is not ok throw the error
    if(!response.ok){
        throw new Error(`Failed to fetch todos: ${response.statusText}`);
    }
    // if response was ok then convert data in json and return
    return response.json();
})
.then( (data) => {
    console.log("Todos Data :", data);
})
.catch( (error) => {
    console.log(error);
});