// import the fetch
const fetch = require('node-fetch');

const todosURL = 'https://jsonplaceholder.typicode.com/todos';

// fetch the data from todosURL
fetch(todosURL)
.then( (response) => {
    // if the response is not ok throw the error
    if(!response.ok){
        throw new Error(`Failed to fetch todos: ${response.statusText}`);
    }
    // if response was ok then convert data in json and return
    return response.json();
})
.then( (data) => {

    const todoDetails = data.filter( (currentObj) => {
        // get the current user id
        const userId = currentObj.userId;

        // if the userId is 1 then fetch details of that user
        if(userId === 1){
            return fetch(`https://jsonplaceholder.typicode.com/todos?userId=${userId}`)
            .then( (response) => {
                // if the response is not ok throw the error
                if(!response.ok){
                    throw new Error(`Failed to fetch users todos details: ${response.statusText}`);        
                }
                // if response was ok then convert data in json and return
                return response.json();
            })
        }
    });

    return Promise.all(todoDetails);
})
.then( (result) => {
    result.forEach( (currentData) => {
        console.log(currentData);
    })
})
.catch( (error) => {
    console.log(error);
});