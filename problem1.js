// import the fetch
const fetch = require('node-fetch');

const userURL = 'https://jsonplaceholder.typicode.com/users';

// fetch the data from userURL
fetch(userURL)
.then( (response) => {
    // if the response is not ok throw the error
    if (!response.ok) {
        throw new Error(`Failed to fetch users: ${response.statusText}`);
    }
    // if response was ok then convert data in json and return
    return response.json();
})
.then( (data) => {
    // print the data
    console.log("Users Data :", data);
})
.catch( (error) => {
    // print the error
    console.log(error);
});