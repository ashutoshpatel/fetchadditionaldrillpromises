// import the fetch
const fetch = require('node-fetch');

const userURL = 'https://jsonplaceholder.typicode.com/users';

// fetch the data from userURL
fetch(userURL)
.then( (response) => {
    // if the response is not ok throw the error
    if(!response.ok){
        throw new Error(`Failed to fetch users: ${response.statusText}`);
    }
    // if response was ok then convert data in json and return
    return response.json();
})
.then( (data) => {

    const userDetails = data.map( (currentObj) => {
        // get the current user id
        const userId = currentObj.id;

        // fetch the data for current user id
        return fetch(`https://jsonplaceholder.typicode.com/users?id=${userId}`)
        .then( (response) => {
            // if the response is not ok throw the error
            if(!response.ok){
                throw new Error(`Failed to fetch users details: ${response.statusText}`);        
            }
            // if response was ok then convert data in json and return
            return response.json();
        })
    });

    return Promise.all(userDetails);
})
.then( (data) => {
    data.forEach( (currentData) => {
        console.log(currentData);
    })
})
.catch( (error) => {
    console.log(error);
});