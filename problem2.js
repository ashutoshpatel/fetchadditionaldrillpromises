// import the fetch
const fetch = require('node-fetch');

const todosURL = 'https://jsonplaceholder.typicode.com/todos';

// fetch the data from todosURL
fetch(todosURL)
.then((response) => {
    // if the response is not ok throw the error
    if (!response.ok) {
        throw new Error(`Failed to fetch todos: ${response.statusText}`);
    }
    // if response was ok then convert data in json and return
    return response.json();
})
.then( (data) => {
    // print the data
    console.log("Todos Data :", data);
})
.catch( (error) => {
    // print the error
    console.log(error);
});